# boinc-docker

It is advised to clone the [Git repository](https://gitlab.com/mausy5043-docker/boinc.git) and build your own image from that.


## Installing
The preferred procedure is:
```
git clone https://gitlab.com/mausy5043-docker/boinc.git
cd boinc
./build.sh Dockerfile.<arch>
./run.sh
```

Replace the `<arch>` above by one of the supported architectures (`amd64` (AMD or Intel), `arm32v7` (Raspberry Pi 3 or newer) or `rpi3` (Raspberry Pi 3 or newer)). Please note that you cannot build for AMD64 on a Raspberry Pi nor can you build an image for a Raspberry Pi on AMD64 architecture. The image must be built on the host itself.

The script will create (if it doesn't already exist) the path `${HOME}/.config/docker/boinc/locale`.
  - `${HOME}/.config/docker/boinc/locale` is needed by BOINC
  - `${HOME}/.config/docker/boinc/` is attached to the Docker container for BOINC to store it's data and configuration non-volatily. This way when you stop the container and later start it again, the BOINC client will continue where it left off.
  - `${HOME}/.config/docker/` is where you may place your custom settings file. The custom settings file must be named `${HOME}/.config/docker/boinc_on_<hostname>-config.txt`, where `<hostname>` must be replaced by the name of the host, obviously. It must contain at least these three parameters:
```
MGR=http://bam.boincstats.com
USR=<username>
PSK=<passkey>
```

`MGR` is the stats manager f.e. `http://bam.boincstats.com`
`USR` is your BOINC-account username
`PSK` is the password/key for your BOINC-account

## Build a fresh image

```
./build.sh Dockerfile.<arch>
```
This builds the image on and for the architecture `<arch>`.


## Run the image

```
./run.sh
```
This runs the container using the image you just built.


## Stop a running container

```
./stop.sh
```
Stops the container and then deletes it. This allows for immediately running a container with the same name without the need to `docker rm` it manually.


## Updating
FIRST!! Make a backup copy of `run.sh` for your own mental health.

```
./update.sh [--all]
```
This force-pulls the current versions of all the files from the remote git repository. Use the `--all` switch to also rebuild the image and restart the container immediately afterwards.
Be aware that this will overwrite/delete any changes you may have made to the files in this folder!

DISCLAIMER:
Use this software at your own risk! We take no responsibility for ANY data loss.
We guarantee no fitness for any use specific or otherwise.
